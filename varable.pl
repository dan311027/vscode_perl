#!/bin/perl


#Variable
$my-name = "John";
$my name = "John";
$my_name = "John"; # Valid variable

#Scalar variable contains a single string or numeric value . It starts with $ symbol
#Syntax: $var_name = value;

$item = "Hello";
$item_one = 2;

#Aray Variable contain a randomnly ordered set of values it start with
#@ symbol

@price_list = (70, 30, 40);
@name_list  = ( "Apple", "Banana", "Guava" );

#Hash Var  contains( key, value ) pair efficiently accessed per key
#it starts with
#% symboii//l

%item_pairs = ("Apple" =>2, "Banana'=>3);
%pair_random = ("Hi" =>8, "Bye"=>9);

#Perl allow var vluest to be modified after var declaration is done
#Scalar $ value can be changed
$name = "John";
#change the_$name
$name ="rahul";

#Array Var can be changed
@array = ( "A", "B", "C", "D", "E" );
#change var 2 (C) 
#defne wth ne var 
@array[2] = "4";
#result is 
@array = ( "A", "B", "4", "D", "E" );


#Hash VAr can be changed

%hash - ("A"), 10, ("C"), 20;
#value 1 need to be modified 13
%hash{10} = 13;

#Variable Interpolation

#Strings that are written with the use of single  `` quotes display the content 
#written within it exactly as it is

$name = "John" 
print 'Hi $name\nHow are you?' 
#The above code will print :
#  Hi $name\nHow are you ?

#with "" double qotas will print the correct name Johs



